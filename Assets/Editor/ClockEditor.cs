﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Clock))]
public class ClockEditor : Editor {
    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        if (GUILayout.Button("Update")) {
            ((Clock)target).UpdateClock();
        }
    }
}