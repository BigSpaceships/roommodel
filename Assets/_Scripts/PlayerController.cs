﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 5f;
    // public Rigidbody rb;
    public float jumpForce = 5f;
    public CharacterController controller;
    public float gravityScale = 1f;

    public Animator anim;
    public Transform pivot;
    private Transform _mainCamera;
    public float rotateSpeed;

    public GameObject playerModel; //drag the child character in here.

    private Vector3 moveDirection;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        _mainCamera = Camera.main.transform;
    }



    // Update is called once per frame
    void Update()
    {
        //We will create a float to store the current Y until after the move is worked out.
        float yStore = moveDirection.y;
        moveDirection = (_mainCamera.forward * Input.GetAxis("Vertical")) + (_mainCamera.right * Input.GetAxis("Horizontal"));
        moveDirection = moveDirection.normalized * moveSpeed;
        moveDirection.y = yStore;

        if (controller.isGrounded)
        {
            moveDirection.y = 0f; //When grounded, gravity will not accumulate on you. More natural.
            if (Input.GetButtonDown("Jump"))
            {
                moveDirection.y = jumpForce;
                //   moveDirection.z = jumpForce;
            }
        }
        moveDirection.y += (Physics.gravity.y * gravityScale * Time.deltaTime);
        controller.Move(moveDirection * Time.deltaTime);

        //Move the player in different directions based on camera look direction.
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            transform.rotation = Quaternion.Euler(0f, pivot.rotation.eulerAngles.y, 0f);
            Quaternion newRotation = Quaternion.LookRotation(new Vector3(moveDirection.x, 0f, moveDirection.z));
            playerModel.transform.rotation = Quaternion.Slerp(playerModel.transform.rotation, newRotation, rotateSpeed * Time.deltaTime);
        }

        anim.SetBool("isGrounded", controller.isGrounded);
        anim.SetFloat("Speed", (Mathf.Abs(Input.GetAxis("Vertical")) + Mathf.Abs(Input.GetAxis("Horizontal"))));
    }
}
