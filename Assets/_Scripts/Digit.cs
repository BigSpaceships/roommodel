using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Digit : MonoBehaviour {
    public enum Number {
        Zero,
        One,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine
    }

    [SerializeField] private GameObject[] segments;

    public Number value;

    private bool[] GetOpenSegments() {
        return value switch {
            Number.Zero => new[] { true, false, true, true, true, true, true },
            Number.One => new[] { false, false, true, false, true, false, false },
            Number.Two => new[] { true, true, true, false, false, true, true },
            Number.Three => new[] { true, true, true, false, true, false, true },
            Number.Four => new[] { false, true, true, true, true, false, false },
            Number.Five => new[] { true, true, false, true, true, false, true },
            Number.Six => new[] { true, true, false, true, true, true, true },
            Number.Seven => new[] { true, false, true, false, true, false, false },
            Number.Eight => new[] { true, true, true, true, true, true, true },
            Number.Nine => new[] { true, true, true, true, true, false, true },
            _ => new[] { false, false, false, false, false, false, false }
        };
    }

    public void UpdateNumber() {
        var enabledSegments = GetOpenSegments();

        for (int i = 0; i < 7; i++) {
            segments[i].SetActive(enabledSegments[i]);
        }
    }
}