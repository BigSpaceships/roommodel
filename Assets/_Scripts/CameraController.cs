﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

    public Transform target;

    public Vector3 offset;

    public bool useOffsetValues; //rather than private controls, 
                                 //this provides a checkbox for unity control.

    public float rotateSpeed;//How fast do we rotate around Player.

    public Transform pivot;

    public float maxViewAngle;
    public float minViewAngle;

    public bool invertY;

    void Start()
    {
        if (!useOffsetValues) // ! measns, does not use
        {
            offset = target.position - transform.position;
        }

        pivot.transform.position = target.transform.position; //As soon as the game starts, lock onto the target(Player)
                                                          
        pivot.transform.parent = null;// unparent the pivot, so we don't rotate the Player with the camera.

        Cursor.lockState = CursorLockMode.Locked;//hide the cursor when the game starts.
       
    }

    void LateUpdate()
    {
        pivot.transform.position = target.transform.position;

        //Get the x position of the mouse @ rotate the target
        float horizontal = Input.GetAxis("Mouse X") * rotateSpeed; //MouseX is the Horizontal
        pivot.Rotate(0, horizontal, 0); //applied to the Camera's Y axis.

        //Get the Y position of the mouse & rotate the pivot.
        float vertical = Input.GetAxis("Mouse Y") * rotateSpeed; //MouseX is the Horizontal

        if (invertY)
        {
            pivot.Rotate(vertical, 0, 0); //applied to the Camera's Y axis.
        }
        else
        {
            pivot.Rotate(-vertical, 0, 0);
        }

        //limit the up/down camera rotation.
        if(pivot.rotation.eulerAngles.x > maxViewAngle && pivot.rotation.eulerAngles.x < 180f)
        {
            pivot.rotation = Quaternion.Euler(maxViewAngle, 0, 0);
        }

        if (pivot.rotation.eulerAngles.x > 180f && pivot.rotation.eulerAngles.x < 360f + minViewAngle)
        {
            pivot.rotation = Quaternion.Euler(360f + minViewAngle, 0, 0);
        }

        //Move the camera based on the current rotation of the target & the original ofset.
        float desiredYAngle = pivot.eulerAngles.y;
        float desiredXAngle = pivot.eulerAngles.x;

        Quaternion rotation = Quaternion.Euler(desiredXAngle, desiredYAngle, 0);
        transform.position = target.position - (rotation * offset);

        //stop the camera from going through the ground when you orbit view toward ground.
        if(transform.position.y < target.position.y)
        {
            transform.position = new Vector3(transform.position.x, target.position.y -.5f, transform.position.z);
        }

        transform.LookAt(target);
    }
}