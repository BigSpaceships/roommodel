﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Playables;

//This Health script is based on 4 health total. If less, change the health cases...
public class HealthManager : MonoBehaviour
{
    public GameObject heart1, heart2, heart3, heart4 /* gameOver*/;
    public int health;
    public int currentHealth;
    public Text gameOverText;

    public GameObject player;
    //public Animator playerAnim;
    public AudioSource lowHealth;
    public AudioClip healthLow;
    public AudioSource hurtPlayer;
    public AudioClip playerHurt;
    public Image gameOverBlack; //Will fade out to black when GameOver.
    private bool isFadeToBlack;
    private bool isFadeFromBlack;
    public float fadeSpeed = 2f;
    public float waitForFade = .5f;
    public float waitForPlayable = 4f;//make this the length of the GameOverTimeline.

    public bool alreadyPlayed = false; //audio in our health case to play before loss of next health
    public bool alreadyPlayed2 = false; //next audio clip
    public bool alreadyPlayed3 = false; //next audio clip

    public float invincibilityLength; //Time of no damage between player being able to take damage.
    private float invincibilityCounter; //Countdown from real time until take damage. 

    private float flashCounter; //Time to coundown until player goes back visible...
    public float flashLength = 0.1f; //Time between flashOn/FlashOff of player renderer...

    private bool isGameOver; //check to see if Game Over Coroutine is already runing...
    public int gameOverWait = 5; //Time to wait before game reloads.

    public PlayableDirector playableDirector;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        // playerAnim = player.GetComponent<Animator>();
        //Health system
        currentHealth = health;
        heart1.gameObject.SetActive(true);
        heart2.gameObject.SetActive(true);
        heart3.gameObject.SetActive(true);
        heart4.gameObject.SetActive(true);
       // gameOver.gameObject.SetActive(false);
        gameOverText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (invincibilityCounter > 0)
        {
            invincibilityCounter -= Time.deltaTime;

            flashCounter -= Time.deltaTime;
            if (flashCounter <= 0)
            {
                player.GetComponentInChildren<Renderer>().enabled = !player.GetComponentInChildren<Renderer>().enabled;
                flashCounter = flashLength;
            }
            if (invincibilityCounter <= 0)
            {
                player.GetComponentInChildren<Renderer>().enabled = true;
            }
        }
        
        if (isFadeToBlack)
        {
            gameOverBlack.color = new Color(gameOverBlack.color.r, gameOverBlack.color.g, gameOverBlack.color.b, Mathf.MoveTowards(gameOverBlack.color.a, 1f, fadeSpeed * Time.deltaTime));
            if (gameOverBlack.color.a == 1f)
            {
                isFadeToBlack = false;
            }
        }
        if (isFadeFromBlack)
        {
            gameOverBlack.color = new Color(gameOverBlack.color.r, gameOverBlack.color.g, gameOverBlack.color.b, Mathf.MoveTowards(gameOverBlack.color.a, 0f, fadeSpeed * Time.deltaTime));
            if (gameOverBlack.color.a == 0f)
            {
                isFadeFromBlack = false;
            }
        }
        
    }
    public void HurtPlayer(int damageToGive)
    {
        if (invincibilityCounter <= 0)
        {
            currentHealth -= damageToGive;

            if (currentHealth <= 0)
            {
                GameOver();
            }
            else
            {
                invincibilityCounter = invincibilityLength;
                player.GetComponentInChildren<Renderer>().enabled = false;
                flashCounter = flashLength;
                SwitchHealth();
            }
        }
    }

    void SwitchHealth() //After there is a check for invinibility and damage, see if heart image -.
    {
        if (health > 4)
            health = 4;
        switch (currentHealth)
        {
            case 4:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(true);
                heart4.gameObject.SetActive(true);
                break;

            case 3:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(true);
                heart4.gameObject.SetActive(false);
                if (!alreadyPlayed)
                {
                    hurtPlayer.PlayOneShot(playerHurt);
                    alreadyPlayed = true;
                }
                break;

            case 2:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(false);
                heart4.gameObject.SetActive(false);
                if (!alreadyPlayed2)
                {
                    hurtPlayer.PlayOneShot(playerHurt);
                    alreadyPlayed2 = true;
                }
                break;

            case 1:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(false);
                heart3.gameObject.SetActive(false);
                heart4.gameObject.SetActive(false);
                if (!alreadyPlayed3)
                {
                    lowHealth.PlayOneShot(healthLow);
                    alreadyPlayed3 = true;
                }
                break;

            case 0:
                heart1.gameObject.SetActive(false);
                heart2.gameObject.SetActive(false);
                heart3.gameObject.SetActive(false);
                heart4.gameObject.SetActive(false);
                break;
        }
    }

    public void HealPlayer(int healAmount)
    {
        currentHealth += healAmount;
        SwitchHealth(); //Go back to change health image + healAmount
    }


    public void GameOver() //This coroutine makes sure game is not already over before running
    {
        if (!isGameOver)
        {
            StartCoroutine("GameOverCo");
        }
    }

    public IEnumerator GameOverCo()
    {
        isGameOver = true;
        gameOverText.text = "You Failed Boot Camp";
        invincibilityCounter = invincibilityLength;
        flashCounter = flashLength;
        player.GetComponentInChildren<Renderer>().enabled = false;




        playableDirector.Play();

        yield return new WaitForSeconds(waitForPlayable);
        LoadMenu();

    }

    public void LoadMenu()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("Menu");
    }

}