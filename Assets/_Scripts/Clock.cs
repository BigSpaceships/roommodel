using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clock : MonoBehaviour {
    [SerializeField] private GameObject digitOne;
    [SerializeField] private Digit digitTwo;
    [SerializeField] private Digit digitThree;
    [SerializeField] private Digit digitFour;

    [Range(1,12)] public int hours;
    [Range(0, 60)] public int minutes;

    public void UpdateClock() {
        digitOne.SetActive(hours / 10 == 1);
        digitTwo.value = (Digit.Number)(hours % 10);
        digitTwo.UpdateNumber();
        
        digitThree.value = (Digit.Number)(minutes / 10);
        digitThree.UpdateNumber();
        digitFour.value = (Digit.Number)(minutes % 10);
        digitFour.UpdateNumber();
    }

    private void Update() {
        hours = DateTime.Now.Hour;
        minutes = DateTime.Now.Minute;
        
        UpdateClock();
    }
}
